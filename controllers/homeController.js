import Controller from './controller.js'

class homeController extends Controller {
    async index(ctx) {
        ctx.render('home.pug', {
            title: "Home page"
        })
    }
}

export default homeController