import Jwt from 'koa-jwt'
import config from '../config/config.js'

export default Jwt({
  secret: config.jwt.secret
})