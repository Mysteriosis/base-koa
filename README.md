# Base Koa
Empty koa base project with scaffolded structure.  
Just copy and start creating new koa based server.

## Dependencies
- MongoDB
- Node v.12

## Installation
- `git clone https://gitlab.com/Mysteriosis/base-koa.git && cd base-koa`
- `yarn install`
- `cp .env.exemple .env`
- `yarn seed`
- `yarn dev`