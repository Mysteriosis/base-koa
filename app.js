import Koa from 'koa'
import Pug from 'koa-pug'
import Logger from 'koa-logger'
import BodyParser from 'koa-bodyparser'
import momentjs from 'moment'

import config from './config/config.js'
import router from './config/routes.js'

const app = new Koa()

// Init middlewares
app.use(Logger())
app.use(BodyParser())
app.use(router.routes())

new Pug({
   viewPath: './views',
   basedir: './views',
   noCache: config.app.env === 'development',
   locals: {
      moment: momentjs
   },
   app: app
})

// Error handler in production
app.use(async (ctx, next) => {
   if(config.app.env === 'production') {
      try {
         await next();
         if ((ctx.status || 404) === 404) {
            ctx.throw(404)
         }
      } catch (err) {
         ctx.status = err.status || 500
         ctx.body = err.message
         ctx.app.emit('error', err, ctx)
      }
   }
})

// Start server
app.listen(config.app.port);